import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableLicenceComponent } from './table-licence.component';

describe('TableLicenceComponent', () => {
  let component: TableLicenceComponent;
  let fixture: ComponentFixture<TableLicenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableLicenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableLicenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
