import {Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren} from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {getAllLicenses} from "../store/license/license-actions";
import {selectLicenses} from "../store/license/license-selectors";
import {License} from "../model/license";

@Component({
  selector: 'app-table-licence',
  templateUrl: './table-licence.component.html',
  styleUrls: ['./table-licence.component.css']
})

export class TableLicenceComponent implements OnInit {
  ngOnInit(): void {
  }
  input: string = ""
  hide: boolean =  false
  licensesArray: License[] = []
  page = 1;
  pageSize = 5;
  collectionSize = this.licensesArray.length;
  licenses: License[] = [];
  licencesCopy: License[] = [];

  refreshLicenses() {
    this.licenses = this.licensesArray
      .map((license, i) => ({id: i + 1, ...license}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor(private store$: Store,
              public dialog: MatDialog,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
  ) {
      this.onInit();
  }

  onInit(){
    this.store$.dispatch(getAllLicenses());
    this.store$.select(selectLicenses).subscribe(result=>{this.licensesArray = result;
      this.collectionSize = result.length; this.licencesCopy = result});
    setTimeout(()=>this.refreshLicenses(),300)

  }

  sortByName(){
    // @ts-ignore
    this.licensesArray.sort((a,b) => (a.name> b.name) ? 1 : ((b.name > a.name) ? -1 : 0))
    this.refreshLicenses()
  }
  sortByActive(){
    // @ts-ignore
    this.licensesArray.sort((a,b) =>  (a.active === b.active)? 0 : a.active? -1 : 1)
    this.refreshLicenses()
  }
  sortByType(){
    // @ts-ignore
    this.licensesArray.sort((a,b) => (a.type?.name> b.type?.name) ? 1 : ((b.type?.name > a.type?.name) ? -1 : 0))
    this.refreshLicenses();
  }

  filterInactive(event: any){
    if (this.hide){
     this.licensesArray = this.licensesArray.filter(x=>x.active === true);
     this.refreshLicenses();
   }
    else {

      this.licensesArray = this.licencesCopy;
      this.refreshLicenses();
    }
  }

  sortById(){
    // @ts-ignore
    this.licensesArray.sort((a,b) => (a.id> b.id) ? 1 : ((b.id > a.id) ? -1 : 0))
    this.refreshLicenses();
  }



  sortByDate(param: String){
    // @ts-ignore
    this.licensesArray.sort(function(a,b) {
      // @ts-ignore
      a = a[param].split('-').join('');
      // @ts-ignore
      b = b[param].split('-').join('');
      return a > b ? 1 : a < b ? -1 : 0;

      // return a.localeCompare(b);         // <-- alternative

    });
    this.refreshLicenses();
  }
  int = 0;
  searchBy(event: any){
    clearTimeout(this.int)

    const input = event.target.value.toLowerCase();
    console.log(input);
    this.int = setTimeout(()=>{
      let lic = this.licensesArray
      this.licensesArray = this.licensesArray.filter(x=>(x.id?.toString().toLowerCase().includes(input) || x.name?.toLowerCase().includes(input) || x.purchaseDate?.toLowerCase().includes(input) ||
        x.type?.name.toLowerCase().includes(input)));
      this.page = 1;
      this.refreshLicenses()
      this.licensesArray = lic;
    },500)

  }

}
