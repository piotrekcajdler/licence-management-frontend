import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {Computer} from "../model/computer";
import {getAllComputers} from "../store/computer/computer-actions";
import {selectComputers} from "../store/computer/computer-selectors";
import {MatDialog} from "@angular/material/dialog";
import {AddComputerDialogComponent} from "../dialogs/add-computer-dialog/add-computer-dialog.component";
import {TableLicenceComponent} from "../table-licence/table-licence.component";
import {  ViewChild, AfterViewInit } from '@angular/core';

import {FormBuilder, Validators} from "@angular/forms";

import {MatDialogRef} from "@angular/material/dialog";
import {createComputer} from "../store/computer/computer-actions";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";

import {TableComputersComponent} from "../table-computers/table-computers.component";
import {TokenStorageService} from "../services/token-storage.service";
import {ComputerService} from "../services/computer.service";
@Component({
  selector: 'app-computer-list',
  templateUrl: './computer-list.component.html',
  styleUrls: ['./computer-list.component.css']
})
export class ComputerListComponent implements OnInit {

  computers: Computer[] | undefined;

  displayedColumns: string[] = ['location', 'name', 'description', 'actions'];

  @ViewChild(TableComputersComponent) table : any;

  constructor(private store$: Store,
             private formBuilder: FormBuilder,
           private modal : NgbModal,private computerService: ComputerService,public tokenStorage: TokenStorageService) {
    this.store$.dispatch(getAllComputers());
    if (!this.tokenStorage.getAuthorities().includes("ROLE_SUPER_ADMIN")){
       this.computerService.getComputerOfUser().subscribe(res=> this.computers= res);
    }
    else {
     this.store$.select(selectComputers).subscribe(res=> this.computers =res);
    }
  }

  ngOnInit(): void {

  }


  addComputerForm = this.formBuilder.group({
    location: ['', Validators.required],
    name: [''],
    isMultiuser: [''],
    description:['']
  });

log(){
  console.log(this.addComputerForm.value.isMultiuser);
}

  addComputer(){

    let computer = {
      name: this.addComputerForm.value.name,
      Location: this.addComputerForm.value.location,
      isMultiuser: this.addComputerForm.value.isMultiuser,
      description: this.addComputerForm.value.description
    }
    console.log(computer)
    this.store$.dispatch(createComputer({computer}));

    this.modal.dismissAll();
    this.table.onInitTable()
  }


  openModal(content: any) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'})
  }

}
