import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {JwtResponse} from "../model/jwt-response";
import {User} from "../model/user";

const backendAddress = "http://localhost:8080";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  loginUser(data: any): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(backendAddress + "/restApi/auth" + "/signin", data);
  }
  signUp(data: any): Observable<any> {
    return this.http.post<any>(backendAddress + "/restApi/auth" + "/signup", data);
  }

  changePassword(data: any): Observable<any> {
    return this.http.patch<any>(backendAddress + "/user/change", data);
  }
  changeCurrentUserPassword(data: any): Observable<any> {
    return this.http.patch<any>(backendAddress + "/user/currentUser/change", data);
  }
  deleteUser(id: any): Observable<any> {
    return this.http.delete<any>(backendAddress + "/user/" + id);
  }
  switchRoles(data: any): Observable<any> {
    return this.http.patch<any>(backendAddress + "/user/switch/" + data, {});
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(backendAddress + "/user" + "/all");
  }
  getAllNotAdmins(): Observable<User[]> {
    return this.http.get<User[]>(backendAddress + "/user" + "/getNotAdmins");
  }

  getAllAdmins(): Observable<User[]> {
    return this.http.get<User[]>(backendAddress + "/user" + "/getAdmins");
  }

  getLogged(): Observable<User> {
    console.log(backendAddress + "/user" + "/getLogged")
    return this.http.get<User>(backendAddress + "/user" + "/getLogged");
  }

  grantByEmail(email: string): Observable<any> {
    return this.http.post<any>(backendAddress + "/restApi/auth" + "/grant/" + email, {});
  }

}
