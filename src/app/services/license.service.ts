import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {License} from "../model/license";

const backendAddress = "http://localhost:8080"

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
}

@Injectable({
  providedIn: 'root'
})
export class LicenseService {

  constructor(private http: HttpClient) { }

  getAllLicenses(): Observable<License[]> {
    return this.http.get<License[]>(backendAddress + "/license" + "/all", httpOptions);
  }

  createLicense(license: any): Observable<License>{
    return this.http.post<License>(backendAddress + "/license" + "/create", license, httpOptions);
  }

  getByName(name:any): Observable<License> {
    return this.http.get<License>(backendAddress + "/license/" + name, httpOptions);
  }
  editLicense(id: number, data: any): Observable<License>{
    return this.http.patch<License>(backendAddress + "/license" + "/update" + "/" + id, data, httpOptions);
  }
}
