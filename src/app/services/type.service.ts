import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Type} from "../model/type"

const backendAddress = "http://localhost:8080"

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  constructor(private http: HttpClient) { }

  getAllTypes(): Observable<Type[]>{
    return this.http.get<Type[]>(backendAddress + "/type" + "/all");
  }

  createType(type: any): Observable<Type> {
    return this.http.post<Type>(backendAddress + "/type" + "/create",type);
  }

}
