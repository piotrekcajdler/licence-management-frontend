import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Computer} from "../model/computer";

const backendAddress = "http://localhost:8080"
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  })
}

@Injectable({
  providedIn: 'root'
})
export class ComputerService {

  constructor(private http: HttpClient) { }

  getAllComputers(): Observable<Computer[]> {
    return this.http.get<Computer[]>(backendAddress + "/computer" + "/all", httpOptions);
  }
  getComputerOfUser(): Observable<Computer[]> {
    return this.http.get<Computer[]>(backendAddress + "/computer" + "/log", httpOptions);
  }

  createComputer(computer: any): Observable<Computer> {
    return this.http.post<Computer>(backendAddress + "/computer" + "/create", computer, httpOptions);
  }
  assignAdminToComputer(computer: number,admin: number): Observable<Computer> {
    return this.http.patch<Computer>(backendAddress +'/computer' + "/assign" + "/" + computer + '/' + admin, {} , httpOptions);
  }

  deleteComputer(id: number): Observable<string> {
    return this.http.delete<string>(backendAddress + "/computer" + "/" + id, httpOptions);
  }

  editComputer(id: number, data: any): Observable<Computer> {
    return this.http.patch<Computer>(backendAddress + "/computer" + "/update" + "/" + id, data, httpOptions);
  }

}
