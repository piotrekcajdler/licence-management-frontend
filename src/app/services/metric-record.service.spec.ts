import { TestBed } from '@angular/core/testing';

import { MetricRecordService } from './metric-record.service';

describe('MetricRecordService', () => {
  let service: MetricRecordService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MetricRecordService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
