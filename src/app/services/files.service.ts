import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
const backendAddress = "http://localhost:8080"
@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private http: HttpClient) { }

  getFilesFromLicense(id: any): Observable<any> {
    return this.http.get<any>(backendAddress + "/file/" + id );
  }
  getAllFiles(): Observable<any> {
    return this.http.get<any>(backendAddress + "/file/all");
  }
  getFile(id: any): Observable<any> {
    // @ts-ignore
    return this.http.get<any>(backendAddress + "/file/getFiles/" + id, {responseType: "blob"}  );
  }

  uploadFile(file:File,id: any): Observable<any> {
    let formData : FormData = new FormData();
    console.log(file)
    formData.append('file',file)


    return this.http.post<FormData>(backendAddress + "/file/addFile/" + id,
      formData
    ,{headers: {"responseType" : "text","Accept" : "*/*", type: 'formData'},}  );
  }

}
