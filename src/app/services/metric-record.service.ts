import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {MetricRecord} from "../model/metric-record";

const backendAddress = "http://localhost:8080"

@Injectable({
  providedIn: 'root'
})
export class MetricRecordService {

  constructor(private http: HttpClient) { }

  getAllMetrics(): Observable<MetricRecord[]>{
    return this.http.get<MetricRecord[]>(backendAddress + "/metric" + "/all");
  }

  createMetric(metric: any): Observable<MetricRecord>{
    return this.http.post<MetricRecord>(backendAddress + "/metric" + "/create", metric);
  }

  getAllMetricsForComputer(id: number): Observable<MetricRecord[]>{
    return this.http.get<MetricRecord[]>(backendAddress + "/metric" + "/comp" + "/" + id);
  }

  getAllMetricsForLicense(id: number): Observable<MetricRecord[]>{
    return this.http.get<MetricRecord[]>(backendAddress + "/metric" + "/lic" + "/" + id);
  }

  editMetric(id: number, data: any): Observable<MetricRecord> {
    return this.http.patch<MetricRecord>(backendAddress + "/metric" + "/update" + "/" + id, data);
  }
  acceptMetric(id: number): Observable<any> {
    return this.http.patch<MetricRecord>(backendAddress + "/metric" + "/accept" + "/all/" + id,{});
  }
  acceptOneMetric(id: number): Observable<any> {
    return this.http.patch<MetricRecord>(backendAddress + "/metric" + "/accept" + "/" + id,{});
  }

}
