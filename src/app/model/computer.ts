import {User} from "./user";

export interface Computer {
  id?: number;
  name?: string;
  location?: string;
  usersOfComputer?: string[];
  workers?: User[];
  isMultiuser?: boolean;
  description?: string;
}
