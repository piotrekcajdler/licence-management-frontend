export interface Institute {
  id?: number;
  name?: string;
  parentID?: number;
}
