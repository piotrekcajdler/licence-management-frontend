import { Type } from "./type";

export interface License {
  id?: number;
  name?: string;
  type?: Type;
  purchaseDate?: string;
  expiryDate?: string;
  active?: boolean;
  availableInstallations?: number;
  key?: string;
  description?: string;
  files?: File[];
}
