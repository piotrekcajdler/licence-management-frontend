import {Computer} from "./computer";
import {Institute} from "./Institute";
import {Role} from "./role";

export interface User {
  id: number;
  email: string;
  password: string;
  name: string;
  surname: string;
  roles: Role[];
  computerList: Computer[],
  institute: Institute
}
