import {License} from "./license";
import {Computer} from "./computer";

export interface MetricRecord {
  id?: number;
  license?: License;
  computer?: Computer;
  installDate?: string;
  uninstallDate?: string;
  whoInstalled?: string;
  whoAgreed?:string;
  whoUninstalled?: string;
  description?:string;
}
