import {Component, OnInit} from '@angular/core';
import {Type} from "../model/type";
import {Store} from "@ngrx/store";
import {createType, getAllTypes} from "../store/type/type-actions";
import {selectTypes} from "../store/type/type-selectors";
import {catchError, Observable, of} from "rxjs";
import {MatTabsModule} from '@angular/material/tabs';
import {User} from "../model/user";
import {AuthService} from "../services/auth.service";
import {selectComputers} from "../store/computer/computer-selectors";
import {Computer} from "../model/computer";
import {ComputerService} from "../services/computer.service";
import {getAllComputers} from "../store/computer/computer-actions";
import {TokenStorageService} from "../services/token-storage.service";
import {FormBuilder, Validators} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  model = "";
  types$: Observable<Type[]> | null = null;
  logged: User | undefined;
  users: User[] | undefined
  notAdmins: User[] = [];
  computers : Computer[] = []
  adminID: number = 0;
  compID: number = 0;
  email:string = "";
  form1:any = "";
  form2:any = "";
  desc: any = '';
  institute:any = ""
  newPassword: String = "";

  addUserForm = this.formBuilder.group({
    name: ['', Validators.required],
    surname:['',Validators.required],
    email:['',Validators.required],
    password: ['', Validators.required]
  });


  constructor(private formBuilder: FormBuilder,
              private store$: Store,
              private authService: AuthService,
              private computerService:ComputerService,
              public tokenStorage: TokenStorageService,
              private toastr: ToastrService,
              private translate: TranslateService) {
    this.store$.dispatch(getAllComputers());
    this.authService.getAllUsers().subscribe(res=>{this.users = res; console.log(res)});
    this.authService.getLogged().subscribe(res=>{this.logged = res; console.log(res); this.institute = res.institute.name});
    this.store$.select(selectComputers).subscribe(res=>{ this.computers = res; console.log(res)});
    this.authService.getAllNotAdmins().subscribe(res=>{this.notAdmins = res; console.log(res)});

  }
  changePass(){

      const passwordData = {
        ID: this.logged?.id,
        password: this.newPassword
      }

    this.authService.changeCurrentUserPassword(passwordData).subscribe()
    window.location.reload();
    this.toastr.success(this.translate.instant("TOASTR.CHANGE_PASS_SUCCESS"));
  }
  assignEmail(event:any){
    this.email = event.target.value;
    console.log(this.email);
  }
  assign(){
    this.authService.grantByEmail(this.email).subscribe();
    this.authService.getAllNotAdmins().subscribe(res=>{this.notAdmins = res; console.log(res)});
  }

  register(){

    let user =  {
      email: this.addUserForm.value.email,
      password: this.addUserForm.value.password,
      name: this.addUserForm.value.name,
      surname: this.addUserForm.value.surname,
      institute: this.logged?.institute?.id,
      role : ['admin']
    }
    this.authService.signUp(user).subscribe();
    location.reload();
  }

  send(){
    this.computerService.assignAdminToComputer(this.compID,this.adminID).pipe(catchError(
      err => of(this.toastr.error(this.translate.instant("TOASTR.ASSIGN_ADMIN_ERROR"))))).subscribe(
      (res) => {
        this.toastr.success(this.translate.instant("TOASTR.ASSIGN_ADMIN_SUCCESS"));
        console.log(res)
      });
  }
  log(event:any, c: any){
    if (c === 'a') this.adminID = event.target.value;
    else this.compID = event.target.value;
    console.log(this.compID + " " + this.adminID)
  }

  ngOnInit(): void {
    this.getType();
  }

  addType(): void {
    const obj = {
      name: this.model,
      description: this.desc
    }
    this.store$.dispatch(createType({typeName: obj}));
  }


 getType(): void {
    this.store$.dispatch(getAllTypes());
    this.types$ = this.store$.select(selectTypes);
  }

}
