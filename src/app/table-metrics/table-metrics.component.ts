import {Component, Input, OnInit} from '@angular/core';
import {License} from "../model/license";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder} from "@angular/forms";
import {getAllLicenses} from "../store/license/license-actions";
import {selectLicenses} from "../store/license/license-selectors";
import {MetricRecord} from "../model/metric-record";
import {
  getAllMetrics,
  getAllMetricsForComputer,
  getAllMetricsForLicense
} from "../store/metric-record/metric-record-actions";
import {selectMetrics} from "../store/metric-record/metric-record-selectors";
import {ActivatedRoute} from "@angular/router";
import {getAllComputers} from "../store/computer/computer-actions";
import {EditMetricRecordDialogComponent} from "../dialogs/edit-metric-record-dialog/edit-metric-record-dialog.component";
import {MetricRecordService} from "../services/metric-record.service";
import {TokenStorageService} from "../services/token-storage.service";

@Component({
  selector: 'app-table-metrics',
  templateUrl: './table-metrics.component.html',
  styleUrls: ['./table-metrics.component.css']
})
export class TableMetricsComponent implements OnInit {

  @Input() licOrComp : string | undefined;
  @Input() pdfView : boolean | undefined;
  id: number;
  ngOnInit(): void {
  }
  metricArray: MetricRecord[] = []
  page = 1;
  pageSize = 5;
  collectionSize = this.metricArray.length;
  licenses: MetricRecord[] = [];
  mertricCopy: MetricRecord[] = [];

  refreshLicenses() {
    this.licenses = this.metricArray
      .map((license, i) => ({id: i + 1, ...license}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor(private store$: Store,
              public dialog: MatDialog,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,private metricService: MetricRecordService,public tokenStorage: TokenStorageService
  ) {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.onInit();
  }
  acceptAll(){
    this.metricService.acceptMetric(this.id).subscribe();
    this.hide = false
    this.filterAwaitingAcceptance()
    this.onInit()
    location.reload()
  }
  hide: boolean =  false

  accept(id: any){
    this.metricService.acceptOneMetric(id).subscribe()
    location.reload();
  }
  filterAwaitingAcceptance(){
    if (this.hide){
      this.metricArray = this.metricArray.filter(x=>x.whoAgreed === '-');
      this.refreshLicenses();
    }
    else {

      this.metricArray = this.mertricCopy;
      this.refreshLicenses();
    }
  }

  onInit(){
    const id = Number(this.route.snapshot.paramMap.get('id'));
    if (this.licOrComp === 'comp')
      this.store$.dispatch(getAllMetricsForComputer({id}));
    else if (this.licOrComp === 'lic')
      this.store$.dispatch(getAllMetricsForLicense({id}));
    this.store$.select(selectMetrics).subscribe(result=>{this.metricArray = result; this.mertricCopy = result
      this.collectionSize = result.length});
    setTimeout(()=>this.refreshLicenses(),500)
  }

  sortBy(param: string){
    // @ts-ignore
    this.metricArray.sort((a,b) => (a[param]> b[param]) ? 1 : ((b[param] > a[param]) ? -1 : 0))
    this.refreshLicenses()
  }
  sortByLicence(){
    // @ts-ignore
    this.metricArray.sort((a,b) => (a.license?.name> b.license?.name) ? 1 : ((b.license?.name > a.license?.name) ? -1 : 0))
    this.refreshLicenses()
  }
  sortByComputer(){
    // @ts-ignore
    this.metricArray.sort((a,b) => (a.computer?.name> b.computer?.name) ? 1 : ((b.computer?.name > a.computer?.name) ? -1 : 0))
    this.refreshLicenses()
  }

  sortByDate(param: String){
    // @ts-ignore
    this.metricArray.sort(function(a,b) {
      // @ts-ignore
      a = a[param].split('-').join('');
      // @ts-ignore
      b = b[param].split('-').join('');
      return a > b ? 1 : a < b ? -1 : 0;

      // return a.localeCompare(b);         // <-- alternative

    });
    this.refreshLicenses();
  }

  openEditMetricRecordDialog(metricRecord: MetricRecord) {
    this.dialog.open(EditMetricRecordDialogComponent, {
      data: metricRecord
    }).afterClosed().subscribe(()=>{setTimeout(()=>{this.onInit()},100)})
  }

  int = 0;
  searchBy(event: any){
    clearTimeout(this.int)
    const input = event.target.value.toLowerCase();
    console.log(input);
    this.int = setTimeout(()=>{
      let lic = this.metricArray
      this.metricArray = this.metricArray.filter(x=>(x.id?.toString().toLowerCase().includes(input) || x.license?.name?.toLowerCase().includes(input) || x.whoInstalled?.toLowerCase().includes(input)
        || x.whoInstalled?.toLowerCase().includes(input)    || x.whoAgreed?.toLowerCase().includes(input) || x.uninstallDate?.toLowerCase().includes(input) ||  x.installDate?.toLowerCase().includes(input)  || x.whoUninstalled?.toLowerCase().includes(input)));
      this.page = 1;
      this.refreshLicenses()
      this.metricArray = lic;
    },500)

  }

  searchByForLic(event: any){
    clearTimeout(this.int)
    const input = event.target.value.toLowerCase();
    this.int = setTimeout(()=>{
      let lic = this.metricArray
      this.metricArray = this.metricArray.filter(x=>(x.id?.toString().toLowerCase().includes(input) || x.computer?.name?.toLowerCase().includes(input) || x.whoInstalled?.toLowerCase().includes(input)
        || x.whoInstalled?.toLowerCase().includes(input)    || x.whoAgreed?.toLowerCase().includes(input) || x.uninstallDate?.toLowerCase().includes(input) ||  x.installDate?.toLowerCase().includes(input)  || x.whoUninstalled?.toLowerCase().includes(input)));
      this.page = 1;
      this.refreshLicenses()
      this.metricArray = lic;
    },500)

  }

}
