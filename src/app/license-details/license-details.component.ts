import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {License} from "../model/license";
import {Store} from "@ngrx/store";
import {selectLicenses} from "../store/license/license-selectors";
import {editLicense, getAllLicenses} from "../store/license/license-actions";
import {MetricRecord} from "../model/metric-record";
import {getAllMetricsForLicense} from "../store/metric-record/metric-record-actions";
import {selectMetrics} from "../store/metric-record/metric-record-selectors";
import {MatDialog} from "@angular/material/dialog";
import {FormBuilder, Validators} from "@angular/forms";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Type} from "../model/type";
import {TypeService} from "../services/type.service";
import {Location} from "@angular/common";
import {TokenStorageService} from "../services/token-storage.service";
import {File} from "../model/file";
import {FilesService} from "../services/files.service";
import {LicenseService} from "../services/license.service";
import jsPDF from "jspdf";
import html2canvas from "html2canvas"

@Component({
  selector: 'app-license-details',
  templateUrl: './license-details.component.html',
  styleUrls: ['./license-details.component.css']
})
export class LicenseDetailsComponent implements OnInit {
  files: any[] = []
  selectedLicense: License = {};
  license : License = {}
  metrics$: Observable<MetricRecord[]>;

  constructor(private route: ActivatedRoute,
              private store$: Store,
              private dialog: MatDialog,
              private formBuilder: FormBuilder,
              private modal : NgbModal,private fileService: FilesService,
              private typeService: TypeService,
              private location: Location,private licenseService: LicenseService,public tokenStorage: TokenStorageService) {
    this.getLicense();
    this.metrics$ = this.store$.select(selectMetrics);
  }

  ngOnInit(): void {
    this.getLicense();
  }
  setFile(event:any){
    this.files = event.target.files;
  }

  getLicense() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.store$.dispatch(getAllLicenses());
    this.store$.dispatch(getAllMetricsForLicense({id}));

    this.store$.select(selectLicenses).subscribe(
      licenses => {this.selectedLicense = licenses.find(x => x.id == id) || {}; this.license =  licenses.find(x => x.id == id) || {}; }
    );

    this.metrics$ = this.store$.select(selectMetrics);
    this.typeService.getAllTypes().subscribe(res=>this.types = res);

  }

  types: Type[] = [];

  nameValue = this.license.name ? "" + this.license.name : "";
  typeValue = this.license.type?.id ? this.license.type.id : "";
  purchaseDateValue = this.license.purchaseDate ? "" + this.license.purchaseDate : "";
  expiryDateValue = this.license.expiryDate ? "" + this.license.expiryDate : "";
  availableInstallationsValue = this.license.availableInstallations ? "" + this.license.availableInstallations : "";
  keyValue = this.license.key ? "" + this.license.key : "";
  descriptionValue = this.license.description ? "" + this.license.description : "";

  editLicenseForm = this.formBuilder.group({
    name: [this.nameValue, Validators.required],
    type:[this.typeValue],
    purchaseDate: [this.purchaseDateValue, Validators.required],
    expiryDate: [this.expiryDateValue, Validators.required],
    availableInstallations: [this.availableInstallationsValue],
    key: [this.keyValue],
    description: [this.descriptionValue]
  })
  addFile(){
    this.licenseService.getByName(this.selectedLicense.name).subscribe(res=>{
      for (const file of this.files){
        this.fileService.uploadFile(file,res.id).subscribe()}})
  }

  download(file: File){
    this.fileService.getFile(file.id).subscribe(res=>{
      const a = window.document.createElement("a")
      a.href = URL.createObjectURL(new Blob([res]))
      a.setAttribute('download',`${file.name}`)
      a.target = "blank"
      document.body.appendChild(a);
      a.click()
    })
  }
  today = new Date();
  editLicense(): void {

    let license = {
      name: this.editLicenseForm.value.name,
      type: Number(this.editLicenseForm.value.type),
      PurchaseDate: this.editLicenseForm.value.purchaseDate,
      ExpiryDate: this.editLicenseForm.value.expiryDate,
      AvailableInstallations: this.editLicenseForm.value.availableInstallations,
      key: this.editLicenseForm.value.key,
      description: this.editLicenseForm.value.description
    }

    this.store$.dispatch(editLicense({id: this.license.id || 0, data: license}));
    this.modal.dismissAll()
    this.addFile()
    this.onInit()
  }
  onInit(){
    this.getLicense();
  }

  open(content: any){
    this.modal.open(content);
  }

  navigateBack(): void {
    this.location.back();
  }

  public downloadMetric(licenseName: string){
    let DATA = document.getElementById('pdfMetric');
    if(!DATA) return;

    DATA.hidden = false;

    html2canvas(DATA).then(canvas => {
      let fileWidth = 208;
      let fileHeight = canvas.height * fileWidth / canvas.width;

      let PDF = new jsPDF('p', 'mm', 'a4');

      const fileUri = canvas.toDataURL('image/png');

      let image = new Image();
      image.src = fileUri;

      image.onload = function (){
        PDF.addImage(image, 'PNG', 0, 0, fileWidth, fileHeight);
        PDF.save('metric-' + licenseName +'.pdf');
      }
    });

    DATA.hidden = true;

  }



}
