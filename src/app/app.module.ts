import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from "@ngrx/store";
import {HeaderComponent} from './header/header.component';
import {LicenseListComponent} from './license-list/license-list.component';
import {licenseReducer} from "./store/license/license-reducer";
import {EffectsModule} from "@ngrx/effects";
import {MatTabsModule} from '@angular/material/tabs';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {LicenseEffects} from "./store/license/license-effects";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from '@angular/material/form-field';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialogModule} from "@angular/material/dialog";
import {AddLicenseDialogComponent} from './dialogs/add-license-dialog/add-license-dialog.component';
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatListModule} from "@angular/material/list";
import {LicenseDetailsComponent} from './license-details/license-details.component';
import {ComputerEffects} from "./store/computer/computer-effects";
import {computerReducer} from "./store/computer/computer-reducer";
import {ComputerListComponent} from './computer-list/computer-list.component';
import {AddComputerDialogComponent} from './dialogs/add-computer-dialog/add-computer-dialog.component';
import {MetricRecordEffects} from "./store/metric-record/metric-record-effects";
import {metricRecordReducer} from "./store/metric-record/metric-record-reducer";
import {ComputerDetailsComponent} from './computer-details/computer-details.component';
import {SettingsComponent} from "./settings/settings.component";
import {MatSelectModule} from "@angular/material/select";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatSidenavModule} from "@angular/material/sidenav";
import {FlexLayoutModule} from "@angular/flex-layout";
import {SidenavListComponent} from './sidenav-list/sidenav-list.component';
import {ToastrModule} from "ngx-toastr";
import {typeReducer} from "./store/type/type-reducer";
import {TypeEffects} from "./store/type/type-effects";
import {AddMetricDialogComponent} from './dialogs/add-metric-dialog/add-metric-dialog.component';
import {EditComputerDialogComponent} from './dialogs/edit-computer-dialog/edit-computer-dialog.component';
import {EditLicenseDialogComponent} from './dialogs/edit-license-dialog/edit-license-dialog.component';
import {EditMetricRecordDialogComponent} from './dialogs/edit-metric-record-dialog/edit-metric-record-dialog.component';
import {NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {TableLicenceComponent} from './table-licence/table-licence.component';
import {TableComputersComponent} from './table-computers/table-computers.component';
import {TableMetricsComponent} from './table-metrics/table-metrics.component';
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {MatExpansionModule} from '@angular/material/expansion';
import { LoginComponent } from './login/login.component';
import {httpInterceptorProviders} from "./services/auth-interceptor";
import {UserViewComponent} from "./user-view/user-view.component";


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LicenseListComponent,
    AddLicenseDialogComponent,
    LicenseDetailsComponent,
    ComputerListComponent,
    AddComputerDialogComponent,
    ComputerDetailsComponent,
    UserViewComponent,
    SettingsComponent,
    SidenavListComponent,
    AddMetricDialogComponent,
    EditComputerDialogComponent,
    EditLicenseDialogComponent,
    EditMetricRecordDialogComponent,
    AddMetricDialogComponent,
    TableLicenceComponent,
    TableComputersComponent,
    TableMetricsComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatExpansionModule,
    MatTabsModule,
    FormsModule,
    NgbModule,
    MatSelectModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature("licenses", licenseReducer),
    StoreModule.forFeature("computers", computerReducer),
    StoreModule.forFeature("metrics", metricRecordReducer),
    StoreModule.forFeature("types", typeReducer),
    EffectsModule.forRoot([]),
    EffectsModule.forFeature([LicenseEffects, ComputerEffects, MetricRecordEffects, TypeEffects]),
    ReactiveFormsModule,
    StoreDevtoolsModule.instrument({name: "Licenses"}),
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule,
    MatListModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    FlexLayoutModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    NgbModule,
    NgbPaginationModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
