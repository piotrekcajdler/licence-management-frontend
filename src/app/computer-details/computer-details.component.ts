import {Component, OnInit, ViewChild} from '@angular/core';
import {Computer} from "../model/computer";
import {Observable} from "rxjs";
import {MetricRecord} from "../model/metric-record";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {editComputer, getAllComputers} from "../store/computer/computer-actions";
import {createMetric, getAllMetricsForComputer} from "../store/metric-record/metric-record-actions";
import {selectMetrics} from "../store/metric-record/metric-record-selectors";
import {selectComputers} from "../store/computer/computer-selectors";
import {MatDialog} from "@angular/material/dialog";
import {License} from "../model/license";
import {getAllLicenses} from "../store/license/license-actions";
import {selectLicenses} from "../store/license/license-selectors";

import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

import {FormBuilder, Validators} from "@angular/forms";
import {TableMetricsComponent} from "../table-metrics/table-metrics.component";
import {Location} from "@angular/common";
import {User} from "../model/user";
import {AuthService} from "../services/auth.service";
import {TokenStorageService} from "../services/token-storage.service";
import {ComputerService} from "../services/computer.service";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";


@Component({
  selector: 'app-computer-details',
  templateUrl: './computer-details.component.html',
  styleUrls: ['./computer-details.component.css']
})
export class ComputerDetailsComponent implements OnInit {

  selectedComputer: Computer = {};
  computer : Computer = {};
  metrics$: Observable<MetricRecord[]>;
  isChecked: boolean | undefined = false;
  logged: User | undefined;
  users: User[] | undefined
  usersS: String[] = []
  admin1 : User[] = [];
  admin2:User[] = [];
  admin1String: String = '';
  admin2String: String = '';
  N: string | undefined = ' ';
  L: string | undefined = ' ';
  U: string[] | undefined = [];
  D: string | undefined = ' ';
  loggedAdmin: string = ''
  loggedAdminCopy:string = ''
  empty: String = "-"
  admins: User[] = []



  constructor(private route: ActivatedRoute,
              private store$: Store,
              private modal: NgbModal,
              private formBuilder: FormBuilder,
              private dialog: MatDialog,private computerService: ComputerService,
              private location: Location,private authService: AuthService,public tokenStorage: TokenStorageService) {
    this.metrics$ = this.store$.select(selectMetrics);
    this.authService.getAllUsers().subscribe(res=>{this.users = res; console.log(res)});
    this.authService.getLogged().subscribe(res=>{this.logged = res; console.log(res); this.loggedAdmin = res.name + " " + res.surname ; this.loggedAdminCopy = res.name + " " + res.surname});
    this.authService.getAllAdmins().subscribe(res=>{
      this.admin1 = res.filter(x=> {return x.name ===  'Konrad'})
      this.admin2 = res.filter(x=> {return x.name ===  'Maciej'})
      this.admin1String = this.admin1[0].name + " " + this.admin1[0].surname
      this.admin2String = this.admin2[0].name + " " + this.admin2[0].surname

      this.admins = res

    })
    this.computerService.getAllComputers().subscribe(c=>{
      // @ts-ignore
      c.find(res=> res.id === Number(this.route.snapshot.paramMap.get('id'))).workers.map(r=>{
        this.usersS.push(r.name + " " + r.surname)
        console.log(this.usersS)
      })
    })
    this.ngOnInit();
  }

  ngOnInit(): void {
    this.getComputer();

    this.metrics$ = this.store$.select(selectMetrics);
    this.computer = this.selectedComputer;
    this.isChecked = this.computer.isMultiuser
    console.log(this.logged,this.users)


    this.L = this.computer.location;
    this.D = this.computer.description
    this.U = this.computer.usersOfComputer;
    this.N = this.computer.name;

  }

  @ViewChild(TableMetricsComponent) metrics : any;
  getComputer() {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.store$.dispatch(getAllComputers());
    this.store$.dispatch(getAllMetricsForComputer({id}));
    this.store$.dispatch(getAllLicenses());

    this.store$.select(selectComputers).subscribe(
      computers => this.computerList = computers
    );

    this.store$.select(selectLicenses).subscribe(
      licenses => this.licenseList = licenses
    );
    this.store$.select(selectComputers).subscribe(computers => {{
      this.selectedComputer = computers.find(c => c.id == id) || {}; this.isChecked = computers.find(c => c.id == id)?.isMultiuser || false

    }
    });
    this.isChecked = this.selectedComputer.isMultiuser
  }
  log(event:any){
    console.log(this.loggedAdmin)
    console.log(event.target.value)
  }


  addMetricRecordFormUninstall = this.formBuilder.group({
    licenseId: ['', Validators.required],
    uninstallDate: ['',Validators.required],
    whoUninstalled: ['',Validators.required],
    whoAgreed: [''],
    whoAgreed1: [''],
    description: ['']
  });


  addMetricRecordForm = this.formBuilder.group({
    licenseId: ['', Validators.required],
    installDate: ['',Validators.required],
    whoInstalled: ['', Validators.required],
    whoAgreed: [''],
    whoAgreed1: [''],
    description: ['']
  });

  licenseList: License[] = [];
  computerList: Computer[] = [];


  addMetricUninstall(){
    let metricRecord = {
      licenseId: this.addMetricRecordFormUninstall.value.licenseId,
      computerId: this.selectedComputer.id,
      uninstallDate: this.addMetricRecordFormUninstall.value.uninstallDate,
      whoUninstalled: this.addMetricRecordFormUninstall.value.whoUninstalled,
      whoAgreed: this.admin2String,
      description: this.addMetricRecordFormUninstall.value.description,
    }
    if (this.tokenStorage.getAuthorities().includes("ROLE_ADMIN") && this.tokenStorage.getAuthorities().length == 1 ){
      metricRecord = {
        licenseId: this.addMetricRecordFormUninstall.value.licenseId,
        computerId: this.selectedComputer.id,
        uninstallDate: this.addMetricRecordFormUninstall.value.uninstallDate,
        whoUninstalled: this.addMetricRecordFormUninstall.value.whoUninstalled,
        whoAgreed: this.empty,
        description: this.addMetricRecordFormUninstall.value.description,
      }
    }

    console.log(metricRecord)
    this.store$.dispatch(createMetric({metricRecord}));
    this.metrics.onInit();
    this.modal.dismissAll()
  }

  openModalUninstall(content:any){
    this.modal.open(content)
  }

  addMetric(){
    let metricRecord = {
      licenseId: this.addMetricRecordForm.value.licenseId,
      computerId: this.selectedComputer.id,
      installDate: this.addMetricRecordForm.value.installDate,
      whoInstalled: this.addMetricRecordForm.value.whoInstalled,
      whoAgreed: this.admin2String,
      description: this.addMetricRecordForm.value.description,
    }
    if (this.tokenStorage.getAuthorities().includes("ROLE_ADMIN") && this.tokenStorage.getAuthorities().length == 1 ){
      metricRecord = {
        licenseId: this.addMetricRecordForm.value.licenseId,
        computerId: this.selectedComputer.id,
        installDate: this.addMetricRecordForm.value.installDate,
        whoInstalled: this.addMetricRecordForm.value.whoInstalled,
        whoAgreed: this.empty,
        description: this.addMetricRecordForm.value.description,
      }
    }
    console.log(metricRecord)

    this.store$.dispatch(createMetric({metricRecord}));
    this.metrics.onInit();
    this.modal.dismissAll()
  }
  openModal(content:any){
    this.loggedAdmin = this.loggedAdminCopy
    this.modal.open(content)
  }

  open(otherContent:any){
    this.loggedAdmin = this.loggedAdminCopy
    this.modal.open(otherContent)
  }

  locationValue = this.computer.location ? "" + this.computer.location : "";
  nameValue = this.computer.name ? "" + this.computer.name : "";
  descriptionValue = this.computer.description ? "" + this.computer.description : "";

  editComputerForm = this.formBuilder.group({
    location: [this.computer.location],
    name: [this.computer.name],
    description: [this.computer.description],
    isMultiuser:[this.computer.isMultiuser],
    users: [this.computer.usersOfComputer]
  })

  editComputer(): void {
    let  computer;
    if (this.editComputerForm.value.usersOfComputer === ""){
       computer = {
        name: this.editComputerForm.value.name,
        Location: this.editComputerForm.value.location,
        IsMultiuser: this.editComputerForm.value.isMultiuser,
        description: this.editComputerForm.value.description,
        usersOfComputer: this.editComputerForm.value.users
      }
    }else {

       computer = {
        name: this.editComputerForm.value.name,
        Location: this.editComputerForm.value.location,
        IsMultiuser: this.editComputerForm.value.isMultiuser,
        description: this.editComputerForm.value.description,
        usersOfComputer: this.editComputerForm.value.users.split(",")
      }
    }
    this.store$.dispatch(editComputer({id: this.selectedComputer.id || 0, data: computer}));
    setTimeout(()=>{this.ngOnInit()},100)

    this.modal.dismissAll();

  }
  navigateBack(): void {
    this.location.back();
  }

  public downloadMetric(licenseName: string){
    let DATA = document.getElementById('pdfMetric');
    if(!DATA) return;

    DATA.hidden = false;

    html2canvas(DATA).then(canvas => {
      let fileWidth = 208;
      let fileHeight = canvas.height * fileWidth / canvas.width;

      let PDF = new jsPDF('p', 'mm', 'a4');

      const fileUri = canvas.toDataURL('image/png');

      let image = new Image();
      image.src = fileUri;

      image.onload = function (){
        PDF.addImage(image, 'PNG', 0, 0, fileWidth, fileHeight);
        PDF.save('metric-' + licenseName +'.pdf');
      }
    });

    DATA.hidden = true;

  }


}
