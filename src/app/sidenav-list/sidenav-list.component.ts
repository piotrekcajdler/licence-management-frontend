import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {TokenStorageService} from "../services/token-storage.service";

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.css']
})
export class SidenavListComponent implements OnInit {

  @Output() sidenavClose = new EventEmitter();

  constructor(public translate: TranslateService,
              public tokenStorage: TokenStorageService) {
    translate.addLangs(['en', 'pl']);
    translate.setDefaultLang('en');

    const browserLang =translate.getBrowserLang();
    translate.use(browserLang?.match(/en|pl/) ? browserLang : 'en');
  }

  ngOnInit(): void {
  }

  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }
  public changeLanguage(lang: string): void {
    this.translate.use(lang.match(/en|pl/) ? lang : 'en');
  }
}
