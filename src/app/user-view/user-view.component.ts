import { Component, OnInit } from '@angular/core';
import {License} from "../model/license";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder} from "@angular/forms";
import {getAllLicenses} from "../store/license/license-actions";
import {selectLicenses} from "../store/license/license-selectors";
import {AuthService} from "../services/auth.service";
import {User} from "../model/user";
import {TokenStorageService} from "../services/token-storage.service";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {

  ngOnInit(): void {
  }
  input: string = ""
  hide: boolean =  false
  licensesArray: User[] = []
  page = 1;
  pageSize = 5;
  collectionSize = this.licensesArray.length;
  licenses: User[] = [];
  licencesCopy: User[] = [];
  getRoles: boolean[] = []
  logged: User | undefined
  refreshLicenses() {
    this.licenses = this.licensesArray
      .map((license) => ({...license}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor(private store$: Store,
              public dialog: MatDialog,
              private modalService: NgbModal,
              private formBuilder: FormBuilder,
              private userService: AuthService,
              private tokenStorage:TokenStorageService,
              private toastr: ToastrService,
              private translate: TranslateService
  ) {
    this.onInit();
  }

  onInit(){
  this.userService.getLogged().subscribe(res=>{this.logged = res})
    this.userService.getAllUsers().subscribe(result=>{this.licensesArray = result;
      this.collectionSize = result.length; this.licencesCopy = result;
    result.forEach(r=>{
     if (r.roles.length === 1){
       r.roles.map(l=>{
         if (l.name === "ROLE_SUPER_ADMIN")
           this.getRoles.push(true)
         else   this.getRoles.push(false)
       })
     }
     else this.getRoles.push(true)
    })
  });
    setTimeout(()=>this.refreshLicenses(),300)
    setTimeout(()=>console.log(this.getRoles),300)
  }

  changePassword(id: any , password :any){
    const passwordData = {
      ID: id,
      password: password
    }
    this.userService.changePassword(passwordData).subscribe();
    location.reload();
    this.toastr.success(this.translate.instant("TOASTR.CHANGE_PASS_SUCCESS"));
  }
  switch(id: any){


    this.userService.switchRoles(id).subscribe();
    location.reload();
    this.toastr.success(this.translate.instant("TOASTR.SWITCH_ROLES_SUCCESS"));
  }
  deleteUser(id:any){
    this.userService.deleteUser(id).subscribe();
    location.reload();
    this.toastr.success(this.translate.instant("TOASTR.USER_DELETE_SUCCESS"));
  }


  int = 0;
  searchBy(event: any){
    clearTimeout(this.int)
    const input = event.target.value.toLowerCase();
    console.log(input);
    this.int = setTimeout(()=>{
      let lic = this.licensesArray
      this.licensesArray = this.licensesArray.filter(x=>(x.id?.toString().toLowerCase().includes(input) || x.name?.toLowerCase().includes(input) || x.email?.toLowerCase().includes(input)
         ));
      this.page = 1;
      this.refreshLicenses()
      this.licensesArray = lic;
    },500)

  }
}
