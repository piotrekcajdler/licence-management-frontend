import { Component, OnInit } from '@angular/core';
import {Computer} from "../model/computer";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder} from "@angular/forms";
import {getAllComputers} from "../store/computer/computer-actions";
import {selectComputers} from "../store/computer/computer-selectors";
import {TokenStorageService} from "../services/token-storage.service";
import {ComputerService} from "../services/computer.service";

@Component({
  selector: 'app-table-computers',
  templateUrl: './table-computers.component.html',
  styleUrls: ['./table-computers.component.css']
})

export class TableComputersComponent implements OnInit {
  ngOnInit(): void {
  }
  computersArray: Computer[] = []
  page = 1;
  pageSize = 5;
  collectionSize = this.computersArray.length;
  computers: Computer[] = [];

  refreshComputers() {
    this.computers = this.computersArray
      .map((computer, i) => ({id: i + 1, ...computer}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  constructor(private store$: Store,
            private computerService: ComputerService, private tokenStorage: TokenStorageService, public dialog: MatDialog,private modalService: NgbModal,private formBuilder: FormBuilder,
  ) {
    this.onInitTable()
  }

  onInitTable(){
    this.store$.dispatch(getAllComputers());
    if (!this.tokenStorage.getAuthorities().includes("ROLE_SUPER_ADMIN")){
      this.computerService.getComputerOfUser().subscribe(res=> this.computersArray= res);
    }
    else {
      this.store$.select(selectComputers).subscribe(res=> this.computersArray =res);
    }
    setTimeout(()=>this.refreshComputers(),300)
  }


  sortBy(param:string){
    // @ts-ignore
    this.computersArray.sort((a,b) => (a[param]> b[param]) ? 1 : ((b[param] > a[param]) ? -1 : 0))
    this.refreshComputers()
  }
  int = 0;
  searchBy(event: any){
    clearTimeout(this.int)
    console.log("SZUKAJKA: " + this.int);
    const input = event.target.value.toLowerCase();
    this.int = setTimeout(()=>{
      let lic = this.computersArray
      this.computersArray = this.computersArray.filter(x=>(x.id?.toString().toLowerCase().includes(input) || x.name?.toLowerCase().includes(input) || x.location?.toLowerCase().includes(input)
        ));
      this.page = 1;
      this.refreshComputers()
      this.computersArray = lic;
    },500)

  }

}
