import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LicenseListComponent} from "./license-list/license-list.component";
import {LicenseDetailsComponent} from "./license-details/license-details.component";
import {ComputerListComponent} from "./computer-list/computer-list.component";
import {ComputerDetailsComponent} from "./computer-details/computer-details.component";
import {SettingsComponent} from "./settings/settings.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', pathMatch: 'full', component: LoginComponent},
  { path: 'licenses', pathMatch: 'full', component: LicenseListComponent },
  { path: 'licenses/:id', component: LicenseDetailsComponent },
  { path: 'computers', pathMatch: 'full', component: ComputerListComponent},
  { path: 'computers/:id', component: ComputerDetailsComponent },
  { path: 'settings', component: SettingsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
