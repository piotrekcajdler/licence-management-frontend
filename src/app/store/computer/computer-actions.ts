import {createAction, props} from "@ngrx/store";
import {Computer} from "../../model/computer";

export enum ComputerActionTypes {
  GET_ALL_COMPUTERS = "[Computer] Get all Computer",
  GET_ALL_COMPUTERS_SUCCESS = "[Computer] Get all Computer success",
  GET_ALL_COMPUTERS_ERROR = "[Computer] Get all Computer error",
  CREATE_COMPUTER = "[Computer] Create Computer",
  CREATE_COMPUTER_SUCCESS = "[Computer] Create Computer success",
  CREATE_COMPUTER_ERROR = "[Computer] Create Computer error",
  DELETE_COMPUTER = "[Computer] Delete Computer",
  DELETE_COMPUTER_SUCCESS = "[Computer] Delete Computer success",
  DELETE_COMPUTER_ERROR = "[Computer] Delete Computer error",
  EDIT_COMPUTER = "[Computer] Edit Computer",
  EDIT_COMPUTER_SUCCESS = "[Computer] Edit Computer success",
  EDIT_COMPUTER_ERROR = "[Computer] Edit Computer error",
}

export const getAllComputers = createAction(
  ComputerActionTypes.GET_ALL_COMPUTERS
);

export const getAllComputersSuccess = createAction(
  ComputerActionTypes.GET_ALL_COMPUTERS_SUCCESS,
  props<{computers: Computer[]}>()
);

export const getAllComputersError = createAction(
  ComputerActionTypes.GET_ALL_COMPUTERS_ERROR
);

export const createComputer = createAction(
  ComputerActionTypes.CREATE_COMPUTER,
  props<{computer: any}>()
);

export const createComputerSuccess = createAction(
  ComputerActionTypes.CREATE_COMPUTER_SUCCESS,
  props<{computer: Computer}>()
);

export const createComputerError = createAction(
  ComputerActionTypes.CREATE_COMPUTER_ERROR
)

export const deleteComputer = createAction(
  ComputerActionTypes.DELETE_COMPUTER,
  props<{id: number}>()
);

export const deleteComputerSuccess = createAction(
  ComputerActionTypes.DELETE_COMPUTER_SUCCESS,
  props<{id: number}>()
);

export const deleteComputerError = createAction(
  ComputerActionTypes.DELETE_COMPUTER_ERROR
)

export const editComputer = createAction(
  ComputerActionTypes.EDIT_COMPUTER,
  props<{id: number, data: any}>()
)

export const editComputerSuccess = createAction(
  ComputerActionTypes.EDIT_COMPUTER_SUCCESS,
  props<{computer: Computer}>()
)

export const editComputerError = createAction(
  ComputerActionTypes.EDIT_COMPUTER_ERROR
)
