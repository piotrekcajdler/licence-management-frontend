import {createEntityAdapter, EntityState} from "@ngrx/entity";
import {License} from "../../model/license";
import {createReducer, on} from "@ngrx/store";
import {getAllComputersSuccess, createComputerSuccess, editComputerSuccess} from "./computer-actions";
import {Computer} from "../../model/computer";


export interface ComputerState extends EntityState<Computer> {}

export const computerAdapter = createEntityAdapter<Computer>();

export const initialState = computerAdapter.getInitialState();

export const computerReducer = createReducer(
  initialState,
  on(getAllComputersSuccess, (state, {computers}) => computerAdapter.setAll(computers, state)),
  on(createComputerSuccess, (state, {computer}) => computerAdapter.setOne(computer, state)),
  on(editComputerSuccess, (state, {computer}) => computerAdapter.upsertOne(computer, state))
)

export const {
  selectAll,
  selectEntities
} = computerAdapter.getSelectors();
