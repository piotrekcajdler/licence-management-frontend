import {Injectable} from "@angular/core";
import {LicenseService} from "../../services/license.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  ComputerActionTypes, createComputerError,
  createComputerSuccess, editComputerError, editComputerSuccess,
  getAllComputersError,
  getAllComputersSuccess
} from "./computer-actions";
import {catchError, map, of, switchMap} from "rxjs";
import {ComputerService} from "../../services/computer.service";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";

@Injectable({providedIn: 'root'})
export class ComputerEffects {

  getAllComputers$ = createEffect(
    () => this.actions$.pipe(
      ofType(ComputerActionTypes.GET_ALL_COMPUTERS),
      switchMap(() => this.computerService.getAllComputers().pipe(
        map(
          computers => getAllComputersSuccess({computers})
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.LOAD_COMP_ERROR"));
          return of(getAllComputersError());
        })
      ))
    )
  )

  createComputer$ = createEffect(
    () => this.actions$.pipe(
      ofType(ComputerActionTypes.CREATE_COMPUTER),
      switchMap(({computer}) => this.computerService.createComputer(computer).pipe(
        map(computer => {
          this.toastr.success(this.translate.instant("TOASTR.ADD_COMP_SUCCESS"))
          return createComputerSuccess({computer})
        }),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.ADD_COMP_ERROR"));
          return of(createComputerError());
        })
      ))
    )
  )

  editComputer$ = createEffect(
    () => this.actions$.pipe(
      ofType(ComputerActionTypes.EDIT_COMPUTER),
      switchMap(({id, data}) => this.computerService.editComputer(id, data).pipe(
        map(computer => {
          this.toastr.success(this.translate.instant("TOASTR.EDIT_COMP_SUCCESS"));
          return editComputerSuccess({computer});
        }),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.EDIT_COMP_ERROR"));
          return of(editComputerError());
        })
      ))
    )
  )

  constructor(private computerService: ComputerService,
              private actions$: Actions,
              private toastr: ToastrService,
              private translate: TranslateService) {}
}
