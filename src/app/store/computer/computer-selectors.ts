import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromStore from "./computer-reducer"

export const computerSelector = createFeatureSelector<fromStore.ComputerState>("computers");

export const selectComputers = createSelector(computerSelector, fromStore.selectAll);

export const selectComputerById = (props: {id: number}) => createSelector(selectComputers, (computers) => {
  return computers[props.id];
});
