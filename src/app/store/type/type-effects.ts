import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  createTypeError,
  createTypeSuccess,
  getAllTypesError,
  getAllTypesSuccess,
  TypeActionTypes
} from "./type-actions";
import {catchError, map, of, switchMap} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TypeService} from "../../services/type.service";
import {TranslateService} from "@ngx-translate/core";

@Injectable({providedIn: 'root'})
export class TypeEffects {

  getAllTypes$ = createEffect(
    () => this.actions$.pipe(
      ofType(TypeActionTypes.GET_ALL_TYPES),
      switchMap(() => this.typeService.getAllTypes().pipe(
        map(
          types => getAllTypesSuccess({types})
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.LOAD_TYPE_ERROR"));
          return of(getAllTypesError());
        })
      ))
    )
  )

  createType$ = createEffect(
    () => this.actions$.pipe(
      ofType(TypeActionTypes.CREATE_TYPE),
      switchMap(({typeName}) => this.typeService.createType(typeName).pipe(
        map(
          (type) => {
            this.toastr.success(this.translate.instant("TOASTR.ADD_TYPE_SUCCESS"));
            return createTypeSuccess({typeModel: type});
          }
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.ADD_TYPE_ERROR"));
          return of(createTypeError());
        })
      )
    )
  ))

  constructor(private typeService: TypeService,
              private actions$: Actions,
              private toastr: ToastrService,
              private translate: TranslateService) {}
}
