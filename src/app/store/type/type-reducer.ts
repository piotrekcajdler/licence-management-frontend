import {createEntityAdapter, EntityState} from "@ngrx/entity";
import {createReducer, on} from "@ngrx/store";
import {Type} from "../../model/type";
import {createTypeSuccess, getAllTypesSuccess} from "./type-actions";


export interface TypeState extends EntityState<Type> {}

export const typeAdapter = createEntityAdapter<Type>();

export const initialState = typeAdapter.getInitialState();

export const typeReducer = createReducer(
  initialState,
  on(getAllTypesSuccess, (state, {types}) => typeAdapter.setAll(types, state)),
  on(createTypeSuccess, (state, {typeModel}) => typeAdapter.setOne(typeModel, state))
)

export const {
  selectAll,
  selectEntities
} = typeAdapter.getSelectors();
