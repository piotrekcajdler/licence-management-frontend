import {createAction, props} from "@ngrx/store";
import {Type} from "../../model/type";

export enum TypeActionTypes {
  GET_ALL_TYPES = "[Type] Get all types",
  GET_ALL_TYPES_SUCCESS = "[Type] Get all types success",
  GET_ALL_TYPES_ERROR = "[Type] Get all types error",
  CREATE_TYPE = "[Type] Create type",
  CREATE_TYPE_SUCCESS = "[Type] Create type success",
  CREATE_TYPE_ERROR = "[Type] Create type error",
}

export const getAllTypes = createAction(
  TypeActionTypes.GET_ALL_TYPES
);

export const getAllTypesSuccess = createAction(
  TypeActionTypes.GET_ALL_TYPES_SUCCESS,
  props<{types: Type[]}>()
);

export const getAllTypesError = createAction(
  TypeActionTypes.GET_ALL_TYPES_ERROR
);

export const createType = createAction(
  TypeActionTypes.CREATE_TYPE,
  props<{typeName: any}>()
);

export const createTypeSuccess = createAction(
  TypeActionTypes.CREATE_TYPE_SUCCESS,
  props<{typeModel: Type}>()
);

export const createTypeError = createAction(
  TypeActionTypes.CREATE_TYPE_ERROR
);
