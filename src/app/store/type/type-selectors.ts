import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromStore from "./type-reducer"

export const typeSelector = createFeatureSelector<fromStore.TypeState>("types");

export const selectTypes = createSelector(typeSelector, fromStore.selectAll);
