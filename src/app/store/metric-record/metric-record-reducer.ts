import {createEntityAdapter, EntityState} from "@ngrx/entity";
import {createReducer, on} from "@ngrx/store";
import {
  createMetricSuccess, editMetricSuccess,
  getAllMetricsForComputerSuccess,
  getAllMetricsForLicenseSuccess,
  getAllMetricsSuccess
} from "./metric-record-actions";
import {MetricRecord} from "../../model/metric-record";


export interface MetricState extends EntityState<MetricRecord> {}

export const metricAdapter = createEntityAdapter<MetricRecord>();

export const initialState = metricAdapter.getInitialState();

export const metricRecordReducer = createReducer(
  initialState,
  on(getAllMetricsSuccess, (state, {metricRecords}) => metricAdapter.setAll(metricRecords, state)),
  on(getAllMetricsForComputerSuccess, (state, {metricRecords}) => metricAdapter.setAll(metricRecords, state)),
  on(getAllMetricsForLicenseSuccess, (state, {metricRecords}) => metricAdapter.setAll(metricRecords, state)),
  on(createMetricSuccess, (state, {metricRecord}) => metricAdapter.setOne(metricRecord, state)),
  on(editMetricSuccess, (state, {metricRecord}) => metricAdapter.upsertOne(metricRecord, state)),
)

export const {
  selectAll,
  selectEntities
} = metricAdapter.getSelectors();
