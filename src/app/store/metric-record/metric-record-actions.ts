import {createAction, props} from "@ngrx/store";
import {MetricRecord} from "../../model/metric-record";

export enum MetricRecordActionTypes {
  GET_ALL_METRICS = "[Metric record] Get all metric records",
  GET_ALL_METRICS_SUCCESS = "[Metric record] Get all metric records success",
  GET_ALL_METRICS_ERROR = "[Metric record] Get all metric records error",
  GET_ALL_METRICS_FOR_COMPUTER = "[Metric record] Get all metrics for computer records",
  GET_ALL_METRICS_FOR_COMPUTER_SUCCESS = "[Metric record] Get all metrics for computer records success",
  GET_ALL_METRICS_FOR_COMPUTER_ERROR = "[Metric record] Get all metrics for computer records error",
  GET_ALL_METRICS_FOR_LICENSE = "[Metric record] Get all metrics for license records",
  GET_ALL_METRICS_FOR_LICENSE_SUCCESS = "[Metric record] Get all metrics for license records success",
  GET_ALL_METRICS_FOR_LICENSE_ERROR = "[Metric record] Get all metrics for license records error",
  CREATE_METRIC = "[Metric record] Create metric record",
  CREATE_METRIC_SUCCESS = "[Metric record] Create metric record success",
  CREATE_METRIC_ERROR = "[Metric record] Create metric record error",
  EDIT_METRIC = "[Metric record] Edit metric record",
  EDIT_METRIC_SUCCESS = "[Metric record] Edit metric record success",
  EDIT_METRIC_ERROR = "[Metric record] Edit metric record error",
}

export const getAllMetrics = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS
);

export const getAllMetricsSuccess = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS_SUCCESS,
  props<{metricRecords: MetricRecord[]}>()
);

export const getAllMetricsError = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS_ERROR
)

export const getAllMetricsForComputer = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS_FOR_COMPUTER,
  props<{id: number}>()
);

export const getAllMetricsForComputerSuccess = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS_FOR_COMPUTER_SUCCESS,
  props<{metricRecords: MetricRecord[]}>()
);

export const getAllMetricsForLicense = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS_FOR_LICENSE,
  props<{id: number}>()
);

export const getAllMetricsForLicenseSuccess = createAction(
  MetricRecordActionTypes.GET_ALL_METRICS_FOR_LICENSE_SUCCESS,
  props<{metricRecords: MetricRecord[]}>()
);

export const createMetric = createAction(
  MetricRecordActionTypes.CREATE_METRIC,
  props<{metricRecord: any}>()
);

export const createMetricSuccess = createAction(
  MetricRecordActionTypes.CREATE_METRIC_SUCCESS,
  props<{metricRecord: MetricRecord}>()
);

export const createMetricError = createAction(
  MetricRecordActionTypes.CREATE_METRIC_ERROR
)

export const editMetric = createAction(
  MetricRecordActionTypes.EDIT_METRIC,
  props<{id: number, data: any}>()
)

export const editMetricSuccess = createAction(
  MetricRecordActionTypes.EDIT_METRIC_SUCCESS,
  props<{metricRecord: MetricRecord}>()
)

export const editMetricError = createAction(
  MetricRecordActionTypes.EDIT_METRIC_ERROR
)
