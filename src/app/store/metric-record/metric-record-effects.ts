import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  createMetricError,
  createMetricSuccess,
  editMetricError,
  editMetricSuccess,
  getAllMetricsError,
  getAllMetricsForComputerSuccess,
  getAllMetricsForLicenseSuccess,
  getAllMetricsSuccess,
  MetricRecordActionTypes
} from "./metric-record-actions";
import {catchError, map, of, switchMap} from "rxjs";
import {MetricRecordService} from "../../services/metric-record.service";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";

@Injectable({providedIn: 'root'})
export class MetricRecordEffects {

  getAllMetrics$ = createEffect(
    () => this.actions$.pipe(
      ofType(MetricRecordActionTypes.GET_ALL_METRICS),
      switchMap(() => this.metricService.getAllMetrics().pipe(
        map(
          metrics => getAllMetricsSuccess({metricRecords: metrics})
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.LOAD_REC_ERROR"));
          return of(getAllMetricsError());
        })
      ))
    )
  )

  createMetric$ = createEffect(
    () => this.actions$.pipe(
      ofType(MetricRecordActionTypes.CREATE_METRIC),
      switchMap(({metricRecord}) => this.metricService.createMetric(metricRecord).pipe(
        map(metric => {
          this.toastr.success(this.translate.instant("TOASTR.ADD_REC_SUCCESS"));
          return createMetricSuccess({metricRecord: metric});
        }),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.ADD_REC_ERROR"));
          return of(createMetricError());
        })
      ))
    )
  )

  getAllMetricsForComputer$ = createEffect(
    () => this.actions$.pipe(
      ofType(MetricRecordActionTypes.GET_ALL_METRICS_FOR_COMPUTER),
      switchMap(({id}) => this.metricService.getAllMetricsForComputer(id).pipe(
        map(
          metrics => getAllMetricsForComputerSuccess({metricRecords: metrics})
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.LOAD_REC_ERROR"));
          return of(getAllMetricsError());
        })
      ))
    )
  )

  getAllMetricsForLicense$ = createEffect(
    () => this.actions$.pipe(
      ofType(MetricRecordActionTypes.GET_ALL_METRICS_FOR_LICENSE),
      switchMap(({id}) => this.metricService.getAllMetricsForLicense(id).pipe(
        map(
          metrics => getAllMetricsForLicenseSuccess({metricRecords: metrics})
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.LOAD_REC_ERROR"));
          return of(getAllMetricsError());
        })
      ))
    )
  )

  editMetric$ = createEffect(
    () => this.actions$.pipe(
      ofType(MetricRecordActionTypes.EDIT_METRIC),
      switchMap(({id, data}) => this.metricService.editMetric(id, data).pipe(
        map(metric => {
          this.toastr.success(this.translate.instant("TOASTR.EDIT_REC_SUCCESS"));
          return editMetricSuccess({metricRecord: metric});
        }),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.EDIT_REC_ERROR"));
          return of(editMetricError());
        })
      ))
    )
  )

  constructor(private metricService: MetricRecordService,
              private actions$: Actions,
              private toastr: ToastrService,
              private translate: TranslateService) {}
}
