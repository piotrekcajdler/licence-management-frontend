import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromStore from "./metric-record-reducer"

export const metricSelector = createFeatureSelector<fromStore.MetricState>("metrics");

export const selectMetrics = createSelector(metricSelector, fromStore.selectAll);

export const selectLicenseById = (props: {id: number}) => createSelector(selectMetrics, (metrics) => {
  return metrics.find(m => m.id == props.id);
});
