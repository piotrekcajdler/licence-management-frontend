import {createAction, props} from "@ngrx/store";
import {License} from "../../model/license";

export enum LicenseActionTypes {
  GET_ALL_LICENSES = "[License] Get all licenses",
  GET_ALL_LICENSES_SUCCESS = "[License] Get all licenses success",
  GET_ALL_LICENSES_ERROR = "[License] Get all licenses error",
  CREATE_LICENSE = "[License] Create license",
  CREATE_LICENSE_SUCCESS = "[License] Create license success",
  CREATE_LICENSE_ERROR = "[License] Create license error",
  EDIT_LICENSE = "[License] Edit license",
  EDIT_LICENSE_SUCCESS = "[License] Edit license success",
  EDIT_LICENSE_ERROR = "[License] Edit license error",
}

export const getAllLicenses = createAction(
  LicenseActionTypes.GET_ALL_LICENSES
);

export const getAllLicensesSuccess = createAction(
  LicenseActionTypes.GET_ALL_LICENSES_SUCCESS,
  props<{licenses: License[]}>()
);

export const getAllLicensesError = createAction(
  LicenseActionTypes.GET_ALL_LICENSES_ERROR
);

export const createLicense = createAction(
  LicenseActionTypes.CREATE_LICENSE,
  props<{license: any}>()
);

export const createLicenseSuccess = createAction(
  LicenseActionTypes.CREATE_LICENSE_SUCCESS,
  props<{license: License}>()
);

export const createLicenseError = createAction(
  LicenseActionTypes.CREATE_LICENSE_ERROR
)

export const editLicense = createAction(
  LicenseActionTypes.EDIT_LICENSE,
  props<{id: number, data: any}>()
)

export const editLicenseSuccess = createAction(
  LicenseActionTypes.EDIT_LICENSE_SUCCESS,
  props<{license: License}>()
)

export const editLicenseError = createAction(
  LicenseActionTypes.EDIT_LICENSE_ERROR
)
