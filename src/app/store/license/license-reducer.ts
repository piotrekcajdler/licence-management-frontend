import {createEntityAdapter, EntityState} from "@ngrx/entity";
import {License} from "../../model/license";
import {createReducer, on} from "@ngrx/store";
import {getAllLicensesSuccess, createLicenseSuccess, editLicenseSuccess} from "./license-actions";


export interface LicenseState extends EntityState<License> {}

export const licenseAdapter = createEntityAdapter<License>();

export const initialState = licenseAdapter.getInitialState();

export const licenseReducer = createReducer(
  initialState,
  on(getAllLicensesSuccess, (state, {licenses}) => licenseAdapter.setAll(licenses, state)),
  on(createLicenseSuccess, (state, {license}) => licenseAdapter.setOne(license, state)),
  on(editLicenseSuccess, (state, {license}) => licenseAdapter.upsertOne(license, state)),
)

export const {
  selectAll,
  selectEntities
} = licenseAdapter.getSelectors();
