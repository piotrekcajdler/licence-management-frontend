import {Injectable} from "@angular/core";
import {LicenseService} from "../../services/license.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {
  createLicenseError,
  createLicenseSuccess, editLicenseError, editLicenseSuccess,
  getAllLicensesError,
  getAllLicensesSuccess,
  LicenseActionTypes
} from "./license-actions";
import {catchError, map, of, switchMap} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";

@Injectable({providedIn: 'root'})
export class LicenseEffects {

  getAllLicenses$ = createEffect(
    () => this.actions$.pipe(
      ofType(LicenseActionTypes.GET_ALL_LICENSES),
      switchMap(() => this.licenseService.getAllLicenses().pipe(
        map(
          licenses => getAllLicensesSuccess({licenses})
        ),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.LOAD_LIC_ERROR"));
          return of(getAllLicensesError());
        })
      ))
    )
  )

  createLicense$ = createEffect(
    () => this.actions$.pipe(
      ofType(LicenseActionTypes.CREATE_LICENSE),
      switchMap(({license}) => this.licenseService.createLicense(license).pipe(
        map(license => {
          this.toastr.success(this.translate.instant("TOASTR.ADD_LIC_SUCCESS"));
          return createLicenseSuccess({license});
        }),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.ADD_LIC_ERROR"));
          return of(createLicenseError());
        })
      ))
    )
  )

  editLicense$ = createEffect(
    () => this.actions$.pipe(
      ofType(LicenseActionTypes.EDIT_LICENSE),
      switchMap(({id, data}) => this.licenseService.editLicense(id, data).pipe(
        map(license => {
          this.toastr.success(this.translate.instant("TOASTR.EDIT_LIC_SUCCESS"));
          return editLicenseSuccess({license});
        }),
        catchError(() => {
          this.toastr.error(this.translate.instant("TOASTR.EDIT_LIC_ERROR"));
          return of(editLicenseError());
        })
      ))
    )
  )

  constructor(private licenseService: LicenseService,
              private actions$: Actions,
              private toastr: ToastrService,
              private translate: TranslateService) {}
}
