import {createFeatureSelector, createSelector} from "@ngrx/store";
import * as fromStore from "./license-reducer"

export const licenseSelector = createFeatureSelector<fromStore.LicenseState>("licenses");

export const selectLicenses = createSelector(licenseSelector, fromStore.selectAll);

export const selectLicenseById = (props: {id: number}) => createSelector(selectLicenses, (licenses) => {
  return licenses.find(l => l.id == props.id);
});
