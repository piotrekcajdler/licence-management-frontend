import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {License} from "../model/license";
import {selectLicenses} from "../store/license/license-selectors";
import {createLicense, getAllLicenses} from "../store/license/license-actions";
import {MatDialog} from "@angular/material/dialog";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Type} from "../model/type";
import {FormBuilder, Validators} from "@angular/forms";
import {TypeService} from "../services/type.service";
import {TableLicenceComponent} from "../table-licence/table-licence.component";
import {  ViewChild, AfterViewInit } from '@angular/core';
import {TokenStorageService} from "../services/token-storage.service";
//import {FileService} from "../services/file-service.service";
import {LicenseService} from "../services/license.service";
import {FilesService} from "../services/files.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-license-list',
  templateUrl: './license-list.component.html',
  styleUrls: ['./license-list.component.css']
})

export class LicenseListComponent implements OnInit {
  files: any[] = []
  types: Type[] = [];
  file: any = ''
  addLicenseForm = this.formBuilder.group({
    name: ['', Validators.required],
    type:[''],
    purchaseDate: ['', Validators.required],
    expiryDate: ['', Validators.required],
    availableInstallations: [''],
    key: [''],
    description:['']
  });

  uploadForm = this.formBuilder.group({
    profile: ['']
  });


  setFile(event:any){
    this.files = event.target.files;

  }

  @ViewChild(TableLicenceComponent) table : any;
  licenses$: Observable<License[]>;

  constructor(private store$: Store,
              public dialog: MatDialog,private modalService: NgbModal,private formBuilder: FormBuilder,
              private typeService : TypeService,private http: HttpClient,private filesService : FilesService,private licenseService: LicenseService,public tokenStorage: TokenStorageService) {
    this.store$.dispatch(getAllLicenses());

    this.licenses$ = this.store$.select(selectLicenses);
  }

  ngOnInit(): void {
    this.typeService.getAllTypes().subscribe(res=>this.types = res);
  }

  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'})
  }

  addLicense(){
    if(this.addLicenseForm.invalid) return;

    let license = {
      name: this.addLicenseForm.value.name,
      type: Number(this.addLicenseForm.value.type),
      PurchaseDate: this.addLicenseForm.value.purchaseDate,
      ExpiryDate: this.addLicenseForm.value.expiryDate,
      AvailableInstallations: this.addLicenseForm.value.availableInstallations,
      key: this.addLicenseForm.value.key,
      description: this.addLicenseForm.value.description
    }

    this.store$.dispatch(createLicense({license}))
    // @ts-ignore
    if (this.files.length !==0){

      setTimeout(()=>{
        this.licenseService.getByName(this.addLicenseForm.value.name).subscribe(res=>{
         for (const file of this.files){
           console.log(file)
           this.filesService.uploadFile(file,res.id).subscribe()
         }

        })
      },1000)
    }
    this.modalService.dismissAll()
    this.table.onInit();

  }



}
