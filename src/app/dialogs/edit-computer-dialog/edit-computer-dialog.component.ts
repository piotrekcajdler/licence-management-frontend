import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Computer} from "../../model/computer";
import {editComputer} from "../../store/computer/computer-actions";

@Component({
  selector: 'app-edit-computer-dialog',
  templateUrl: './edit-computer-dialog.component.html',
  styleUrls: ['./edit-computer-dialog.component.css']
})
export class EditComputerDialogComponent implements OnInit {


  locationValue = this.computer.location ? "" + this.computer.location : "";
  nameValue = this.computer.name ? "" + this.computer.name : "";
  descriptionValue = this.computer.description ? "" + this.computer.description : "";

  editComputerForm = this.formBuilder.group({
    location: [this.locationValue],
    name: [this.nameValue],
    description: [this.descriptionValue],
    users: [""]
  })

  constructor(private formBuilder: FormBuilder,
              private store$: Store,
              private dialogRef: MatDialogRef<EditComputerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private computer: Computer) { }

  ngOnInit(): void {
  }

  editComputer(): void {

    let computer = {
      name: this.editComputerForm.value.name,
      Location: this.editComputerForm.value.location,
      IsMultiuser: true,
      description: this.editComputerForm.value.description,
      usersOfComputer: this.editComputerForm.value.users.split(",")
    }

    this.store$.dispatch(editComputer({id: this.computer.id || 0, data: computer}));
    this.close();
  }

  close(): void {
    this.dialogRef.close();
  }

}
