import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMetricDialogComponent } from './add-metric-dialog.component';

describe('AddMetricDialogComponent', () => {
  let component: AddMetricDialogComponent;
  let fixture: ComponentFixture<AddMetricDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMetricDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMetricDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
