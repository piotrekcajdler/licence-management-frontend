import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {FormBuilder, Validators} from "@angular/forms";
import {createMetric} from "../../store/metric-record/metric-record-actions";
import {License} from "../../model/license";
import {Computer} from "../../model/computer";
import {getAllComputers} from "../../store/computer/computer-actions";
import {getAllLicenses} from "../../store/license/license-actions";
import {selectComputers} from "../../store/computer/computer-selectors";
import {selectLicenses} from "../../store/license/license-selectors";

@Component({
  selector: 'app-add-metric-dialog',
  templateUrl: './add-metric-dialog.component.html',
  styleUrls: ['./add-metric-dialog.component.css']
})
export class AddMetricDialogComponent implements OnInit {

  addMetricRecordForm = this.formBuilder.group({
    licenseId: ['', Validators.required],
    installDate: ['', Validators.required],
    uninstallDate: [''],
    whoInstalled: ['', Validators.required],
    whoUninstalled: [''],
    description: ['']
  });

  licenseList: License[] = [];
  computerList: Computer[] = [];

  constructor(private dialogRef: MatDialogRef<AddMetricDialogComponent>,
              private store$: Store,
              private formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) private computerId: number) { }

  ngOnInit(): void {
    this.store$.dispatch(getAllComputers());
    this.store$.dispatch(getAllLicenses());

    this.store$.select(selectComputers).subscribe(
      computers => this.computerList = computers
    );

    this.store$.select(selectLicenses).subscribe(
      licenses => this.licenseList = licenses
    );
  }

  addMetric(){
    let metricRecord = {
      licenseId: this.addMetricRecordForm.value.licenseId,
      computerId: this.computerId,
      installDate: this.addMetricRecordForm.value.installDate,
      uninstallDate: this.addMetricRecordForm.value.uninstallDate,
      whoInstalled: this.addMetricRecordForm.value.whoInstalled,
      whoUninstalled: this.addMetricRecordForm.value.whoUninstalled,
      description: this.addMetricRecordForm.value.description,
    }

    console.log(metricRecord);

    this.store$.dispatch(createMetric({metricRecord}));
    this.close();
  }

  close(){
    this.dialogRef.close();
  }

}
