import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {FormBuilder, Validators} from "@angular/forms";
import {createLicense} from "../../store/license/license-actions";
import {TypeService} from "../../services/type.service";
import {Type} from "../../model/type";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-license-dialog',
  templateUrl: './add-license-dialog.component.html',
  styleUrls: ['./add-license-dialog.component.css']
})
export class AddLicenseDialogComponent implements OnInit {
  types: Type[] = [];


  addLicenseForm = this.formBuilder.group({
    name: ['', Validators.required],
    type:[''],
    purchaseDate: ['', Validators.required],
    expiryDate: ['', Validators.required],
    availableInstallations: [''],
    key: [''],
    description: ['']
  });

  constructor(private dialogRef: MatDialogRef<AddLicenseDialogComponent>,
              private store$: Store,
              private formBuilder: FormBuilder,
              private typeService : TypeService,private modalService: NgbModal,) { }

  ngOnInit(): void {
    this.typeService.getAllTypes().subscribe(res=>this.types = res);
  }

  close(){
    this.dialogRef.close();
  }

  addLicense(){
    if(this.addLicenseForm.invalid) return;

    let license = {
      name: this.addLicenseForm.value.name,
      type: Number(this.addLicenseForm.value.type),
      PurchaseDate: this.addLicenseForm.value.purchaseDate,
      ExpiryDate: this.addLicenseForm.value.expiryDate,
      AvailableInstallations: this.addLicenseForm.value.availableInstallations,
      key: this.addLicenseForm.value.key,
      description: this.addLicenseForm.value.description
    }

    this.store$.dispatch(createLicense({license}));

    this.close();
  }


}
