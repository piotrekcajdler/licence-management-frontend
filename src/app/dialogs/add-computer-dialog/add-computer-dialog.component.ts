import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";
import {MatDialogRef} from "@angular/material/dialog";
import {createComputer} from "../../store/computer/computer-actions";
import {ModalDismissReasons, NgbModal} from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: 'app-add-computer-dialog',
  templateUrl: './add-computer-dialog.component.html',
  styleUrls: ['./add-computer-dialog.component.css']
})
export class AddComputerDialogComponent implements OnInit {

  addComputerForm = this.formBuilder.group({
    location: ['', Validators.required],
    name: [''],
    description: [''],
    isMultiuser: [''],
  });

  constructor(private formBuilder: FormBuilder,
              private store$: Store,
              private dialogRef: MatDialogRef<AddComputerDialogComponent>,private modal : NgbModal) { }

  ngOnInit(): void {
  }

  log(){
    console.log(this.addComputerForm.value.IsMultiuser)
  }
  addComputer(){
    let computer = {
      name: this.addComputerForm.value.name,
      Location: this.addComputerForm.value.location,
      IsMultiuser: true,
      description: this.addComputerForm.value.description
    }

    console.log(computer);

    this.store$.dispatch(createComputer({computer}));

    this.modal.dismissAll();
  }


  openModal(content: any) {
    this.modal.open(content, {ariaLabelledBy: 'modal-basic-title'})
  }
}
