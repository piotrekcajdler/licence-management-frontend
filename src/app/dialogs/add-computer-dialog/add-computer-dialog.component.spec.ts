import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddComputerDialogComponent } from './add-computer-dialog.component';

describe('AddComputerDialogComponent', () => {
  let component: AddComputerDialogComponent;
  let fixture: ComponentFixture<AddComputerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddComputerDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComputerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
