import {Component, Inject, Input, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, Validators} from "@angular/forms";
import {MetricRecord} from "../../model/metric-record";
import {createMetric, editMetric} from "../../store/metric-record/metric-record-actions";

@Component({
  selector: 'app-edit-metric-record-dialog',
  templateUrl: './edit-metric-record-dialog.component.html',
  styleUrls: ['./edit-metric-record-dialog.component.css']
})
export class EditMetricRecordDialogComponent implements OnInit {


  installDateValue = this.metricRecord.installDate ? this.metricRecord.installDate : "";
  uninstallDateValue = this.metricRecord.uninstallDate ? this.metricRecord.uninstallDate : "";
  whoInstalledValue = this.metricRecord.whoInstalled ? this.metricRecord.whoInstalled : "";
  whoUninstalledValue = this.metricRecord.whoUninstalled ? this.metricRecord.whoUninstalled : "";
  descriptionValue = this.metricRecord.description ? this.metricRecord.description : "";

  editMetricRecordForm = this.formBuilder.group({
    installDate: [this.installDateValue, Validators.required],
    uninstallDate: [this.uninstallDateValue],
    whoInstalled: [this.whoInstalledValue, Validators.required],
    whoUninstalled: [this.whoUninstalledValue],
    description: [this.descriptionValue]
  });

  constructor(private store$: Store,
              private dialogRef: MatDialogRef<EditMetricRecordDialogComponent>,
              private formBuilder: FormBuilder,
              @Inject(MAT_DIALOG_DATA) private metricRecord: MetricRecord) { }

  ngOnInit(): void {
  }

  editMetric() {
    let metricRecord = {
      licenseId: this.metricRecord.license?.id || 0,
      computerId: this.metricRecord.computer?.id || 0,
      installDate: this.editMetricRecordForm.value.installDate,
      uninstallDate: this.editMetricRecordForm.value.uninstallDate,
      whoInstalled: this.editMetricRecordForm.value.whoInstalled,
      whoUninstalled: this.editMetricRecordForm.value.whoUninstalled,
      description: this.editMetricRecordForm.value.description
    }

    console.log(metricRecord)
    console.log(this.editMetricRecordForm.value.description)
    if(!this.metricRecord.id) return;
      this.store$.dispatch(editMetric({id: this.metricRecord.id || 0, data: metricRecord}))


    this.close();
  }


  close() {
    this.dialogRef.close();
  }

}
