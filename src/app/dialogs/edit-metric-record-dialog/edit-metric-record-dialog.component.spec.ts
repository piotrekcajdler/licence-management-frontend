import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMetricRecordDialogComponent } from './edit-metric-record-dialog.component';

describe('EditMetricRecordDialogComponent', () => {
  let component: EditMetricRecordDialogComponent;
  let fixture: ComponentFixture<EditMetricRecordDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMetricRecordDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMetricRecordDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
