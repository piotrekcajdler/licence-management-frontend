import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Store} from "@ngrx/store";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {License} from "../../model/license";
import {editLicense} from "../../store/license/license-actions";
import {Type} from "../../model/type";
import {TypeService} from "../../services/type.service";

@Component({
  selector: 'app-edit-license-dialog',
  templateUrl: './edit-license-dialog.component.html',
  styleUrls: ['./edit-license-dialog.component.css']
})
export class EditLicenseDialogComponent implements OnInit {

  types: Type[] = [];

  nameValue = this.license.name ? "" + this.license.name : "";
  typeValue = this.license.type?.id ? this.license.type.id : "";
  purchaseDateValue = this.license.purchaseDate ? "" + this.license.purchaseDate : "";
  expiryDateValue = this.license.expiryDate ? "" + this.license.expiryDate : "";
  availableInstallationsValue = this.license.availableInstallations ? "" + this.license.availableInstallations : "";
  keyValue = this.license.key ? "" + this.license.key : "";
  descriptionValue = this.license.description ? "" + this.license.description : "";

  editLicenseForm = this.formBuilder.group({
    name: [this.nameValue, Validators.required],
    type:[this.typeValue],
    purchaseDate: [this.purchaseDateValue, Validators.required],
    expiryDate: [this.expiryDateValue, Validators.required],
    availableInstallations: [this.availableInstallationsValue],
    key: [this.keyValue],
    description: [this.descriptionValue]
  })

  constructor(private formBuilder: FormBuilder,
              private store$: Store,
              private dialogRef: MatDialogRef<EditLicenseDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private license: License,
              private typeService: TypeService) { }

  ngOnInit(): void {
    this.typeService.getAllTypes().subscribe(res => this.types = res);
  }

  editLicense(): void {

    let license = {
      name: this.editLicenseForm.value.name,
      type: Number(this.editLicenseForm.value.type),
      PurchaseDate: this.editLicenseForm.value.purchaseDate,
      ExpiryDate: this.editLicenseForm.value.expiryDate,
      AvailableInstallations: this.editLicenseForm.value.availableInstallations,
      key: this.editLicenseForm.value.key,
      description: this.editLicenseForm.value.description
    }

    this.store$.dispatch(editLicense({id: this.license.id || 0, data: license}));
    this.close();
  }

  close(): void {
    this.dialogRef.close();
  }

}
