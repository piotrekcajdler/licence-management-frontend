import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {TokenStorageService} from "../services/token-storage.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();

  constructor(public translate: TranslateService,
              public tokenStorage: TokenStorageService) {
    translate.addLangs(['en', 'pl']);
    translate.setDefaultLang('en');

    const browserLang =translate.getBrowserLang();
    translate.use(browserLang?.match(/en|pl/) ? browserLang : 'en');
  }

  ngOnInit(): void {
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  public changeLanguage(lang: string): void {
    this.translate.use(lang.match(/en|pl/) ? lang : 'en');
  }
}
